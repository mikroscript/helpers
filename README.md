# MikroScript Helpers

This is a collection of helper scripts primarily used by MikroScript projects. They are not intended to be installed independently. Feel free to use the helpers in your own scripts. They should be pretty self-explanatory.