{
  # param: in
  # param: replace
  # param: with

  :local checkMatch do={
    # param: in
    # param: match
    # param: startIndex
    # returns: 1 if match found, 0 otherwise

    :if (($startIndex + ([:len $match] - 1)) > [:len $in]) do={
      :return 0;
    }

    :for i from=0 to=([:len $match] - 1) do={

      :local char [:pick $in ($startIndex + $i)];
      :local matchChar [:pick $match $i];

      :if ($char != $matchChar) do={
        :return 0;
      }
    }

    :return 1;
  }

  :if ([:len $in] < [:len $replace]) do={
    :return $in;
  }

  :local result "";

  :local i 0;
  :while ($i < [:len $in]) do={
    :if ([$checkMatch in=$in match=$replace startIndex=$i] = 1) do={
      :set result ($result . $with);
      :set i ($i + [:len $replace]);
    } else={
      :set result ($result . [:pick $in $i]);
      :set i ($i + 1);
    }
  }

  :return $result;
}