{
  :local read do={:return};

  :local result [$read];

  :return $result;
}