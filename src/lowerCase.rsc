{
  # param: in

  :local lower "abcdefghijklmnopqrstuvwxyz";
  :local upper "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  :local result "";

  :for i from=0 to=([:len $in] - 1) do={

    :local char [:pick $in $i];
    :local pos [:find $upper $char];

    :if ($pos > -1) do={
      :set char [:pick $lower $pos];
    }

    :set result ($result . $char);
  }

  :return $result;
}