{
  # param: in
  # param: replace
  # param: with

  :local result "";

  :for i from=0 to=([:len $in] - 1) do={
    
    :local char [:pick $in $i];

    :if ($char = $replace) do={
      :set char $with;
    }

    :set result ($result . $char);
  }

  :return $result;
}